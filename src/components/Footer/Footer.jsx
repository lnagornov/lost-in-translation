import React from 'react';

import "./Footer.module.css";


const Footer = () => {
    return (
        <footer>
            <p>© 2022 Lev Nagornov</p>
        </footer>
    );
};

export default Footer;