import React from "react";

import classes from "./Loader.module.css";


const Loader = () => {
    return (
        <div className={classes.centered}>
            <div className={classes.loader}>
                <span>L</span>
                <span>O</span>
                <span>A</span>
                <span>D</span>
                <span>I</span>
                <span>N</span>
                <span>G</span>
            </div>
        </div>
    );
};

export default Loader;