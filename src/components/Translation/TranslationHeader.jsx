import React from 'react';

import classes from "./TranslationHeader.module.css";


const TranslationHeader = () => {
    return (
        <section className={classes.TranslationHeader}>
            <img src={`${process.env.PUBLIC_URL}/img/Logo-Hello.png`} alt="RobotHello" className={classes.robotHello}/>
            <div className={classes.robotText}>
                <h4><span>></span>Type something in English in the field below.</h4>
                <h4><span>></span>And I will translate it into American sign language!</h4>
                <h4><span>></span>=^._.^=</h4>
            </div>
        </section>
    );
};

export default TranslationHeader;