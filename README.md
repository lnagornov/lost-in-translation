<div align="center">
    <h1>Noroff Assignment 2: Frontend Development with React</h1>
    <img src="https://i.ibb.co/FH1P3fH/1-c-Ph7uj-RIfc-HAy4k-W2-ADGOw.png" width="128" alt="React">
</div>

[![license](https://img.shields.io/badge/License-MIT-green.svg)](LICENSE)
[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

## Lost in translation

Sign language translator as a Single Page Application built using the React framework. Made for Assignment #2 Noroff JS course.

Deployed with heroku [here](https://radiant-beyond-70216.herokuapp.com).

The application have one main feature: to act as a “translator” from regular text to sign language.
It allows translating English words and short sentences to American sign language.

Firstly you need to log into app with your username. You could use any name, no password is required.
Then you need type in the word and press the "translate" button or press "Enter", you will see the result below.
You can find a history of your translations on the Profile page. It will save the last 10 translations.
The history can be cleared using the "clear history" button.

## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Setup](#setup)
* [License](#license)
* [Author](#author)

## General info
This front-end project contains:
* Login
* Translation
* Profile

## Technologies
Project is created with:
* HTML
* CSS
* React 18


## Setup
To run the project, download files and open folder of the project and run these commands:
```bash
npm install
npm start
```

## License

[MIT](https://choosealicense.com/licenses/mit/)

## Author
Lev Nagornov

---
2022 Made for Noroff JS course
